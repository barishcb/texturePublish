""" Base testCase class for the project texturePublish
"""
import os as _os
import tempfile as _tempfile

from bcTesting import testBase


class TexturePublishTestCase(testBase.TestCase):
	""" Base testCase for the projecte texturePublish
	"""
	def setUp(self):
		super(TexturePublishTestCase, self).setUp()
		self._testInputPath = self.getResources("textures")
		self._tempDirectory = _tempfile.mkdtemp()

	def getResources(self, *args):
		return _os.path.join(
			_os.path.dirname(__file__),
			"resources",
			*args
		)
