""" Testing for texturePublish.utilities.files module
"""
import os as _os

from texturePublish import exceptions as _exceptions
from texturePublish.utilities import files as _filesUtils

from tests import testBase

class Test_Files(testBase.TexturePublishTestCase):
	""" Tests for function in texturePublish.utilities.files module
	"""
	def test_collectTextues_valid(self):
		""" Testing collectTextures function with valid path
		"""
		result = _filesUtils.collectTextures(self._testInputPath)

		self.assertEqual(8, len(result))

	def test_collectTextues_emptyDirectory(self):
		""" Testing collectTextures function with empty directory
		"""
		self.assertRaises(_exceptions.UtilityError, _filesUtils.collectTextures, self._tempDirectory)

	def test_getOutputPath_valid(self):
		""" Testing getOutputPath function with valid file path
		"""
		textures = _filesUtils.collectTextures(self._testInputPath)
		result = _filesUtils.getOutputPath(textures[0], self._tempDirectory)
		expected = _os.path.join(self._tempDirectory, "renderman", "COL", "1001.tx")

		self.assertEqual(expected, result)

	def test_getOutputPath_wrongFileName(self):
		""" Testing getOutputPath function with valid file path
		"""
		self.assertRaises(
			_exceptions.ValidationError,
			_filesUtils.getOutputPath,
			"C:\\wrongfilename.exr",
			self._tempDirectory
		)

