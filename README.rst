Some basic tools that help to release/publish textures for Autodesk Maya and The Foundry Katana from Mari, to use renderman

:Author: `Barish Chandran B <http://www.barishcb.com>`_

.. contents::
    :local:
    :depth: 2
    :backlinks: none

.. sectnum::

Introduction
============

What is texturePublish?
----------------

``texturePublish`` conatins some basic tools release/publish textures for Autodesk Maya and The Foundry Katana from Mari, to use renderman.

How is texturePublish licensed?
-------------------------------

`MIT License <https://opensource.org/licenses/MIT>`_

Contact details
---------------

Drop me an email to barishcb@gmail.com for any questions regarding ``texturePublish``. For reporting problems with ``texturePublish`` or submitting feature requests, the best way is to open an issue on the `texturePublish project page <https://gitlab.com/barishcb/texturePublish>`_.

Installation
============

Prerequisites
-------------

* ``texturePublish`` was tested only on Python 2.7, PyQt4 and SIP on both Linux and Windows. It should work on any later version as well.

Installation From Source
------------------------

Installing ``texturePublish`` is very simple. Once you download and unzip the package, you just have to execute the standard ``python setup.py install``. The setup script will then place the ``texturePublish`` module into ``site-packages`` in your Python's installation library.

Setps using normal python ::

    > cd <development_path>
    > mkdir tmp
    > cd tmp
    > git clone https://gitlab.com/barishcb/texturePublish.git
    > cd texturePublish
    > python setup.py build
    > python setup.py test
    > python setup.py install

Setps using gnu make ::

    > cd <development_path>
    > mkdir tmp
    > cd tmp
    > git clone https://gitlab.com/barishcb/texturePublish.git
    > cd testing
    > make build
    > make test
    > make install

HOW TO USE
==========

How to use texturePublish Commandline Tool
------------------------------------------

.. code-block:: bash

    > texturePublishCLI --texturePath /any/valid/mari/texture/path/ --outputPath /any/output/directory --proxy

* texturePath

    Give a valid Mari texture path, image file foramat must be in <layername>.<udim_number>.<extension>
        eg:
            COL.1001.exr
            SPEC.1002.exr

* outputPath

    Give a writable directory path where the converted textures will be saved. The folder stucture given below.

.. code-block:: bash

    outputpath
      `-- geometryName
           `-- layer1
                 `-- 1001.tx
* proxy

    Add this argument if the proxy image also need for preview purpose.

Mari Tool Demo
==============

`Demo Video  <docs/source/video/toolScreenGrab.mp4>`_

Package contents
================

Once you unzip the ``texturePublish`` package, you'll see the following files and directories:

README.rst:
  This README file.

setup.py:
  Installation script

texturePublish/:
  The ``texturePublish`` module source code.

tests/:
  Unit tests.

Contributors
============

`Barish Chandran B <http://www.barishcb.com>`_
