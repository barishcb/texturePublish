"""
"""
import mari

defaultTextureNameTemplate = "$CHANNEL_$UDIM.tif"

defaultTextureDirectory = "texture"
defaultOutputDirectory = "output"

# Export Options
saveOptions = mari.Image.DEFAULT_OPTIONS | mari.Image.ENABLE_FULL_PATCH_BLEED
