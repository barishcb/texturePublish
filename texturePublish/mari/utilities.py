""" Mari utilities for texture publish tool
"""
import os

import mari

import common as _common
reload(_common)


def getChannels(allObject=False):
    """ Collect all channels for the selected obeject or all objects.

        Keyword Args:
            allObject (bool): Add all the existing channels in the current project (default: {False})

        Returns:
            (list): List of tuples with geo name and channel names
    """
    geometries = sorted(mari.geo.list(), key=lambda geo: geo.name())
    channels = []
    selectedChannels = []
    notSelectedChannels = []
    for geometry in geometries:
        sortedChannels = sorted(geometry.channelList(), key=lambda channel: channel.name())
        if geometry is mari.geo.current():
            selectedChannels.append((geometry.name(), sortedChannels))
        elif allObject:
            notSelectedChannels.append((geometry.name(), sortedChannels))
    channels.extend(selectedChannels)
    channels.extend(notSelectedChannels)
    return channels


def exportChannels(channels, textureRootPath, nameTemplate=None):
    """ Export given channels to the texturePath

        # ToDo : Add more options to export
        Args:
            channels (list): List of channels
            textureRootPath (str): Path as string
    """
    nameTemplate = nameTemplate or _common.defaultTextureNameTemplate

    for channel in channels:
        texturePath = getTexturePath(textureRootPath, channel)
        if not os.path.exists(texturePath):
            os.makedirs(texturePath)

        mari.app.log("Starting converting %s : %s" % (channel.name(), texturePath))
        channel.exportImagesFlattened(
            os.path.join(texturePath, nameTemplate),
            _common.saveOptions,
            []
        )
        mari.app.log("Finished converting %s : %s" % (channel.name(), texturePath))


def getTexturePath(projectPath=None, channel=None):
    """ [summary]

        [description]

        Keyword Args:
            projectPath ([type]): [description] (default: {None})
    """
    if not projectPath:
        for project in mari.projects.list():
            tempPath = os.path.dirname(project.projectPath())
            if tempPath and os.path.exists(tempPath):
                projectPath = tempPath

    texturePath = projectPath or os.getcwd()

    if os.path.basename(texturePath) != _common.defaultTextureDirectory:
        texturePath =  os.path.join(projectPath, _common.defaultTextureDirectory)

    if channel:
        geometry = channel.geoEntity()
        texturePath = os.path.join(texturePath, geometry.name())
    return texturePath


def getOutputPath(projectPath=None):
    """ [summary]

        [description]

        Keyword Args:
            projectPath ([type]): [description] (default: {None})
    """
    if not projectPath:
        for project in mari.projects.list():
            tempPath = os.path.dirname(project.projectPath())
            if tempPath and os.path.exists(tempPath):
                projectPath = tempPath

    projectPath = projectPath or os.getcwd()
    outputPath = os.path.join(projectPath, _common.defaultOutputDirectory)
    if not os.path.exists(outputPath):
        os.makedirs(outputPath)

    return outputPath
