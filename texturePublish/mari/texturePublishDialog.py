""" Texture Publish dialog for mari
"""
import mari

from PySide import QtCore
from PySide import QtGui

from texturePublish.gui import resources
from texturePublish._core import texturePublishManager
from texturePublish.mari import common as _common
from texturePublish.mari import utilities as _mariUtilities
reload(_common)
reload(_mariUtilities)

class TexturePublishDialog(QtGui.QDialog):
	def __init__(self, parent=None):
		super(TexturePublishDialog, self).__init__(parent)

		self._channels = []
		self._selectectChannels = []
		self._createProxy = False
		self._convertToRenderMan = False

		self._ui = resources.Ui_TexturePublish()
		self._ui.setupUi(self)

		# Rename Window
		self.setWindowTitle('Texture Publish tool For Mari')

		self.__connectSignals()
		self.__loadValues()

	def __connectSignals(self):
		""" Connects widget signals
		"""
		# Channels Options
		self._ui.channelsAddPushButton.released.connect(self.__addChannels)
		self._ui.channelsRemovePushButton.released.connect(self.__removeChannels)

		# Destination Path Options
		self._ui.convertRManGroup.toggled.connect(self.__convertRManToggled)
		self._ui.texturePathBrowserButton.released.connect(self.__getTexturePath)
		self._ui.outputPathBrowserButton.released.connect(self.__getOutputPath)
		self._ui.createProxyCheckBox.toggled.connect(self.__createProxyToggled)

		# Main Button
		self._ui.exportAllButton.released.connect(self.__exportAll)
		self._ui.exportSelectedButton.released.connect(self.__exportSelected)
		self._ui.closeButton.released.connect(self.close)

	def __loadValues(self):
		""" Loading values to the user interface in the begining
		"""
		self._loadChannels(allObject=True)
		self._ui.texturePathLineEdit.setText(_mariUtilities.getTexturePath())
		self._ui.outputPathLineEdit.setText(_mariUtilities.getOutputPath())

		self._ui.nameTemplateLineEdit.setText(_common.defaultTextureNameTemplate)

		self._createProxy = bool(self._ui.createProxyCheckBox.isChecked())
		self._convertToRenderMan = bool(self._ui.convertRManGroup.isChecked())

	def _loadChannels(self, allObject=False):
		""" Load the channels to the list widgets

			Keyword Args:
				allObject (bool): Add all the existing channels in the current project (default: {False})
		"""
		geoChannelMap = _mariUtilities.getChannels(allObject)
		channelNames = []
		self._ui.channelsListWidget.clear()
		for geometry, channels in geoChannelMap:
			for channel in channels:
				channelName = ("%s:%s" % (geometry, channel.name()))
				self._ui.channelsListWidget.addItem(channelName)
				self._ui.channelsListWidget.item(
					self._ui.channelsListWidget.count() - 1).setData(QtCore.Qt.UserRole, channel
				)

	def selectedChannels(self, allObject=False, asItem=False):
		"""
		"""
		channels = []
		listWidget = self._ui.selectedChannelsListWidget if not allObject else self._ui.channelsListWidget
		for index in xrange(listWidget.count()):
			if asItem:
				channels.append(listWidget.item(index))
			else:
				channels.append(listWidget.item(index).data(QtCore.Qt.UserRole))
		return channels

	def __addChannels(self):
		""" Slot method for the button channelsAddButton.

			Add the selected channels from the Channels list widget to
			Selected channels list widget
		"""
		selectedItems = self._ui.channelsListWidget.selectedItems()
		selectedChannels = [selectedItem.data(QtCore.Qt.UserRole) for selectedItem in selectedItems]
		existingChannels = self.selectedChannels()
		existingChannels.extend(selectedChannels)
		existingChannels = sorted(set(existingChannels), key=lambda channel: channel.name())

		self._ui.selectedChannelsListWidget.clear()
		for channel in existingChannels:
			geometry = channel.geoEntity()
			channelName = ("%s:%s" % (geometry.name(), channel.name()))
			self._ui.selectedChannelsListWidget.addItem(channelName)
			self._ui.selectedChannelsListWidget.item(
				self._ui.selectedChannelsListWidget.count() - 1).setData(QtCore.Qt.UserRole, channel
			)

	def __removeChannels(self):
		""" Slot method for the button channelsRemoveButton.

			Remove the selected channels from the "Selected Channels" list widget
		"""
		selectedItems = reversed(self._ui.selectedChannelsListWidget.selectedItems())
		for selectedItem in selectedItems:
			self._ui.selectedChannelsListWidget.takeItem(
				self._ui.selectedChannelsListWidget.row(selectedItem)
			)

	def __getTexturePath(self):
		""" Slot method for the button texturePathBrowserButton.
		"""
		texturePath = QtGui.QFileDialog.getExistingDirectory(
			self,
			"Export Path",
			self._ui.texturePathLineEdit.text()
		)
		if texturePath:
			self._ui.texturePathLineEdit.setText(texturePath)

	def __convertRManToggled(self, value):
		"""
		"""
		self._convertToRenderMan = value

	def __createProxyToggled(self, value):
		"""
		"""
		self._createProxy = value

	def __getOutputPath(self):
		""" Slot method for the button outputPathBrowserButton.
		"""
		outputPath = QtGui.QFileDialog.getExistingDirectory(
			self,
			"Export Path",
			self._ui.outputPathLineEdit.text()
		)
		if outputPath:
			self._ui.outputPathLineEdit.setText(outputPath)

	def __exportAll(self):
		""" Slot method for the button exportAllButton.
		"""
		self.export(self.selectedChannels(allObject=True))

	def __exportSelected(self):
		""" Slot method for the button exportSelectedButton.
		"""
		self.export(self.selectedChannels(allObject=False))

	def export(self, channels):
		"""
		"""
		mari.app.setWaitCursor()
		texturePath = str(self._ui.texturePathLineEdit.text())
		_mariUtilities.exportChannels(
			channels,
			texturePath
		)

		if self._convertToRenderMan:
			convertManager = texturePublishManager.TexturePublishManager(
				texturePath,
				str(self._ui.outputPathLineEdit.text()),
				self._createProxy
			)
			convertManager.collectTextures()
			convertManager.convert()
		mari.app.restoreCursor()


dialog = TexturePublishDialog()
dialog.show()
