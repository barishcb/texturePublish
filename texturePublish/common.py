""" Common variables used inside texturePublish package
"""
import os as _os

rmanTree = _os.getenv("RMANTREE")
txMakeBinaries = (
	"bin/txmake",
	"rmantree/bin/txmake"
)

supportedTextures = ("exr", "tif", "tiff")
txMakeSettings = {
	"smode": "periodic",
	"tmode": "periodic",
	"resize": "up-"
}

typeExtensionMap = {
	"rman": "tx",
	"renderman": "tx",
	"proxy": "jpg"
}

textureFileNamePattern = "^(?P<layer>\w+)_(?P<udim>\d+).(?P<extension>\w+)$"
