""" Entry point functions for all tools in texturePublish package
"""
from texturePublish._core import texturePublishCLI
from texturePublish.utilities import logger as _logger

_log = _logger.getLogger()


def texturePublish():
	""" Entry point for texturePublish Commandline tool.
	"""
	_log.info("Initializing texturePublish tool")
	texturePublishCLI.run()
