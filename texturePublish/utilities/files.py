""" Public module texturePublish.utilites.files
"""
import os as _os
import glob as _glob
import re as _re

from texturePublish import common as _common
from texturePublish import exceptions as _exceptions


def collectTextures(inputDirectory, fileTypes=None):
	""" Collect all texture files and filter based on the the types
		given

		Args:
			inputDirectory(str): A valid directory path to textures.

		Kwargs:
			fileTypes(list): List of texture file types

		Returns:
			(list) list of textures path
	"""
	fileTypes = fileTypes or _common.supportedTextures
	textureFiles = []

	for fileType in fileTypes:
		for geometry in _os.listdir(inputDirectory):
			textureFiles.extend(
				_glob.glob(
					_os.path.join(
						inputDirectory,
						geometry,
						"*.%s" % fileType
					)
				)
			)

	if not textureFiles:
		raise _exceptions.UtilityError(
			"The given directory doesn't contains any textues of types (%s) in %s" % (
				", ".join(fileTypes),
				inputDirectory
			)
		)
	return sorted([_os.path.realpath(filePath) for filePath in textureFiles])


def getOutputPath(inputPath, outputDirectory, outputType=None):
	""" Function create a output file path from the input file.

		Args:
			inputPath(str): A valid texture file path
			outputDirectory(str): A existing output directory with write permission

		Kwargs:
			outputType(str): type of output (rman or proxy)

		Returns:
			(str) A output file path for the given type of texture
	"""
	fileName = _os.path.basename(inputPath)
	geometryName = _os.path.basename(_os.path.dirname(inputPath))
	texrureNameRegEx = _re.compile(_common.textureFileNamePattern)
	match = texrureNameRegEx.match(fileName)

	if not match:
		raise _exceptions.ValidationError(
			"The file name of inputPath is not matching with the texture pattern('LAYER.1001.ext')"
		)

	details = match.groupdict()
	outputExt = _common.typeExtensionMap.get(outputType, "tx")
	return _os.path.realpath(
		_os.path.join(
			outputDirectory,
			outputType or "renderman",
			geometryName,
			details.get("layer", ""),
			"%s.%s" % (details.get("udim"), outputExt)
		)
	)
