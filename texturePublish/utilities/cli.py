"""
"""
import subprocess

from texturePublish import exceptions as _exceptions


def runCommand(executable, commandArgs=None):
	""" Run command line process from python

		Args:
			executable(str): path to executable

		Kwargs:
			commandArgs(list): list of arguments as string

		Returns:
			(tuple) Return the command status as integer and standard out
	"""
	commandArgs = [r"%s" % executable] + commandArgs or []

	process = subprocess.Popen(
		commandArgs,
		shell=True,
		stdin=subprocess.PIPE,
		stdout=subprocess.PIPE,
		stderr=subprocess.PIPE,
	)
	stdout, stderr = process.communicate()

	if stderr:
		raise _exceptions.UtilityError(
			"Error while running command : %s \n\n Error: %s" % (
				command,
				stderr
			)
		)

	return (process.returncode, stdout)
