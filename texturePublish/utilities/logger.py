""" Setup python logger for the package.
"""
import logging

_cachedLoggers = {}

def getLogger(name=None, level=None):
	""" Function to setup logging for the project and return

		Args:
			name(str): Name of the logger to get
			level(int): Level of logging

		Returns:
			(logger) Return logger for the project
	"""
	level = level or logging.DEBUG
	name = name or "texturePublish"
	if name in _cachedLoggers:
		return _cachedLoggers.get(name)

	# create logger
	logger = logging.getLogger(name)
	logger.setLevel(level)

	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	consoleHandler = logging.StreamHandler()
	consoleHandler.setLevel(level)
	consoleHandler.setFormatter(formatter)

	logger.addHandler(consoleHandler)
	_cachedLoggers[name] = logger

	return logger
