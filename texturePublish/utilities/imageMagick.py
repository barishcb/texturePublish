""" Utility function related to ImageMagick
"""
import os as _os

from texturePublish.utilities import cli as _cliUtils
from texturePublish.utilities import logger as _logger

_log = _logger.getLogger()

def createProxy(inputPath, outputPath, settings=None):
	""" Convert given texture to a proxy texture.

		Args:
			inputPath(str): A valid texture file path
			outputPath(str): A valid proxy image output file path

		Kwargs:
			settings(dict): ImageMagick convert settings as dictionary

		Returns:
			(str) converted texture file path.
	"""
	settings = settings or {}

	# Create output directory if it doesn't exists
	outputDirectory = _os.path.dirname(outputPath)
	if not _os.path.exists(outputDirectory):
		_os.makedirs(outputDirectory)

	commandArgs = []

	for key, value in settings:
		commandArgs.append("-%s" % key)
		commandArgs.append(value)

	commandArgs.append(inputPath)
	commandArgs.append(outputPath)

	_log.info("Converting to proxy : %s -> %s", inputPath, outputPath)
	return _cliUtils.runCommand("convert", commandArgs)

