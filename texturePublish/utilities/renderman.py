""" Utilities function for pixar renderman
"""
import os as _os
import shutil as _shutil

from texturePublish import common as _common
from texturePublish import exceptions as _exceptions
from texturePublish.utilities import cli as _cliUtils
from texturePublish.utilities import logger as _logger

_log = _logger.getLogger()


def getTxMakeExecutable(rmanTree=None):
	""" Function to find txmake executable from renderman
		installation path.

		Kwargs:
			rmanTree (str): Path to renderman installation

		Return:
			(str): Path for the

		Raises:
			UtilityError: When the function unable to find txmake executable
	"""
	rmanTree = rmanTree or _common.rmanTree

	for filePath in _common.txMakeBinaries:
		executable = _os.path.realpath(
			_os.path.join(rmanTree, filePath)
		)
		if _os.name == "nt":
			executable = "%s.exe" % executable
		if _os.path.exists(executable):
			return executable

	raise _exceptions.UtilityError("Unable to find txmake executable")


def convertTexture(inputPath, outputPath, txmakeExec=None, settings=None):
	""" Convert given texture to pixar renderman supported texture.

		Args:
			inputPath(str): A valid texture file path
			outputPath(str): A valid tx output file path

		Kwargs:
			txmakeExec(str): A valid executable file for pixar txmake
			settings(dict): txmake settings as dictionary

		Returns:
			(str) converted texture file path.
	"""
	settings = settings or {}

	# Create output directory if it doesn't exists
	outputDirectory = _os.path.dirname(outputPath)
	if not _os.path.exists(outputDirectory):
		_os.makedirs(outputDirectory)

	txmakeExec = txmakeExec or getTxMakeExecutable()
	commandArgs = []

	for key, value in settings:
		commandArgs.append("-%s" % key)
		commandArgs.append(value)

	if inputPath.endswith("exr"):
		commandArgs.extend(
			["-format", "openexr", "-extraargs", "displaywindow"]
		)
	commandArgs.append(inputPath)
	commandArgs.append(outputPath)

	_log.info("Converting to PXR tx : %s -> %s", inputPath, outputPath)
	return _cliUtils.runCommand(txmakeExec, commandArgs)
