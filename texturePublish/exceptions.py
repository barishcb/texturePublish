""" Custom exceptions used for texturePublish tool
"""
class ValidationError(Exception):
	""" Custom exception for the texturePublish validations
	"""


class ArgumentError(Exception):
	""" Custom exception for the texturePublish commandline argument error
	"""


class UtilityError(Exception):
	""" Custom exception for the utilities
	"""
