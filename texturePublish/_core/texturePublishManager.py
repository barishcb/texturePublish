""" Core manager class for texturePublish tool
"""
import os as _os

from texturePublish import exceptions as _exceptions
from texturePublish.utilities import files as _filesUtils
from texturePublish.utilities import imageMagick as _imUtils
from texturePublish.utilities import renderman as _rmanUtils


class TexturePublishManager(object):
	""" Core manager class for texturePublish tool
		Args:
			texturePath(str): A valid path for input mari textures
			outputPath(str): A valid path for output the tx and proxy files

		Kwargs:
			createProxy(boolean): True if proxy needs to be created.
	"""
	def __init__(self, texturePath, outputPath, createProxy=False):
		super(TexturePublishManager, self).__init__()
		self._texturePath = ""
		self._outputPath = ""
		self._createProxy = False
		self._textureFiles = []
		self._txmakeExec = _rmanUtils.getTxMakeExecutable()

		self.texturePath = texturePath
		self.outputPath = outputPath
		self.createProxy = createProxy

	@property
	def texturePath(self):
		""" Getter method for texturePath property

			Returns:
				(str) A valid path for input mari textures
		"""
		return self._texturePath

	@texturePath.setter
	def texturePath(self, texturePath):
		""" Setter method for texturePath property

			Args:
				texturePath(str): A valid path for input mari textures
		"""
		if not _os.path.exists(texturePath):
			raise _exceptions.ValidationError("The texture path given is not exists.")
		self._texturePath = texturePath
		self.collectTextures()

	@property
	def outputPath(self):
		""" Getter method for outputPath property

			Returns:
				(str) A valid path for output the tx and proxy files
		"""
		return self._outputPath

	@outputPath.setter
	def outputPath(self, outputPath):
		""" Setter method for outputPath property

			Args:
				outputPath(str): A valid path for output the tx and proxy files
		"""
		if not _os.path.exists(outputPath):
			raise _exceptions.ValidationError("The output path given is not exists.")
		self._outputPath = outputPath

	@property
	def createProxy(self):
		""" Getter method for createProxy property

			Returns:
				(boolean) True if proxy needs to be created.
		"""
		return self._createProxy

	@createProxy.setter
	def createProxy(self, createProxy):
		""" Setter method for createProxy property

			Args:
				createProxy(boolean): True if proxy needs to be created.
		"""
		self._createProxy = createProxy

	def collectTextures(self):
		""" Function to collect all texture files inside
			the texrure path given and store it.
		"""
		self._textureFiles = _filesUtils.collectTextures(self.texturePath)

	def convert(self):
		""" Main function to convert and save the texture files
			to the output path.
		"""
		txOutputFiles = []
		proxyOutputFiles = []
		for texturePath in self._textureFiles:
			txOutputPath = _filesUtils.getOutputPath(texturePath, self.outputPath, "renderman")
			result, _ = _rmanUtils.convertTexture(
				texturePath,
				txOutputPath,
				txmakeExec=self._txmakeExec
			)
			txOutputFiles.append(txOutputPath if not result else "")
			if self.createProxy:
				proxyFilePath = _filesUtils.getOutputPath(texturePath, self.outputPath, "proxy")
				result, _ = _imUtils.createProxy(texturePath, proxyFilePath)
				proxyOutputFiles.append(proxyFilePath if not result else "")

		return self._textureFiles, txOutputFiles, proxyOutputFiles
