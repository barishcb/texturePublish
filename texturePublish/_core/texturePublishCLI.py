""" Command line module for texturePublish tool.
"""
import os as _os
import sys as _sys
import argparse as _argparse

from texturePublish import exceptions as _exceptions
from texturePublish._core import texturePublishManager


def texturePublish(texturePath, outputPath, createProxy=False):
	""" Main function to create the manager and run the convert texture function

		Args:
			texturePath(str): A valid path for input mari textures
			outputPath(str): A valid path for output the tx and proxy files

		Kwargs:
			createProxy(boolean): True if proxy needs to be created.
	"""
	tpManager = texturePublishManager.TexturePublishManager(
		texturePath, outputPath, createProxy
	)
	textureFiles, txOutputFiles, proxyOutputFiles = tpManager.convert()


def _validateArgs(parser):
	""" Function to validate all the arguments parsed to the parser

		Args:
			parser (argparse.Namespace) Parser object contains all the arguments

		Return:
			parser (argparse.Namespace) Parser object contains all the arguments

		Raises:
			_exceptions.ArgumentError: When any argumnet parsed is wrong.
	"""
	if not parser.texturePath:
		raise _exceptions.ArgumentError("The texture path should not be empty.")

	if not _os.path.exists(parser.texturePath):
		raise _exceptions.ArgumentError("The texture path given is not exists.")

	if not parser.outputPath:
		parser.outputPath = parser.texturePath
	if not _os.path.exists(parser.outputPath):
		raise _exceptions.ArgumentError("The output path given is not exists.")

	return parser


def _getParser(args):
	"""	Function take arguments and create a argparse parser object
		out of it.

		Args:
			args (list): List string arguments(Mostly system arguments)

		Returns:
			(argparse.Namespace) Parser object contains all the arguments
	"""
	usage = "Tool to publish the textures exported from mari to maya or katana"
	parser = _argparse.ArgumentParser(description=usage)
	parser.add_argument("-tp", "--texturePath", help="The path where the mari textures are saved.")
	parser.add_argument("-op", "--outputPath", nargs='?', help="The path wehere the convered textures will be saved")
	parser.add_argument("-p", "--proxy", action="store_true", help="Convert a proxy output")
	return parser.parse_args(args)


def run():
	""" Initialize function for texturePublish tool.
	"""
	parser = _getParser(_sys.argv[1:])
	parser = _validateArgs(parser)
	texturePublish(
		parser.texturePath,
		parser.outputPath,
		createProxy=parser.proxy
	)


# Testing purpose
if __name__ == '__main__':
	run()
